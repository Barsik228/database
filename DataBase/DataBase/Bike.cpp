#include "Bike.h"
Bike* createBike(int wheels, int power){
	Bike* newBike;
	newBike->wheels = wheels;
	newBike->power = power;
	return newBike;
}

void saveBike(FILE* f, Bike* bike){
	fprintf(f, "%d %d %d", BIKE, bike->wheels, bike->power);
	fclose(f);
}