#include "Functions.h"

Base* createBase(void * head, int length){
	Base* newBase;
	newBase->head = head;
	newBase->length = length;
	return newBase;
}
Element* createElement(void * machine, int type, int id, Element* next){
	Element* newElement;
	newElement->machine = machine;
	newElement->type = type;
	newElement->id = id;
	newElement->next = next;
	return newElement;
}
Base* addElement(Base* base, Element* elem){
	elem->next = base->head;
	base->head = elem;
	return base;
}
void removeElement(int id);
void saveBase(FILE * f, Base* base);
Base* loadBase(char* path);
