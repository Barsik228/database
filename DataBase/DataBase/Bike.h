#pragma once

#include <stdio.h>

#define BIKE 1

typedef struct{
	int wheels;
	int power;
} Bike;

Bike* createBike(int wheels, int power);
void saveBike(FILE* f, Bike* bike);

