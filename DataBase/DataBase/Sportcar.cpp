#include "Sportcar.h"
Sportcar* createSportcar(int wheels, int power){
	Sportcar* newSportcar;
	newSportcar->wheels = wheels;
	newSportcar->power = power;
	return newSportcar;
}

void saveSportcar(FILE* f, Sportcar* sportcar){
	fprintf(f, "%d %d %d", SPORTCAR, sportcar->wheels, sportcar->power);
	fclose(f);
}