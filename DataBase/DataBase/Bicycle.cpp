#include "Bicycle.h"
Bicycle* createBicycle(int wheels, int power){
	Bicycle* newBicycle;
	newBicycle->wheels = wheels;
	newBicycle->power = power;
	return newBicycle;
}

void saveBicycle(FILE* f, Bicycle* bicycle){
	fprintf(f, "%d %d %d", BICYCLE, bicycle->wheels, bicycle->power);
	fclose(f);
}