#pragma once

#include <stdio.h>

#define SPORTCAR 4

typedef struct{
	int wheels;
	int power;
} Sportcar;

Sportcar* createSportcar(int wheels, int power);
void saveSportcar(FILE* f, Sportcar* sportcar);

