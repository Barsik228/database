#pragma once

#include <stdio.h>

#define SCOOTER 3

typedef struct{
	int wheels;
	int power;
} Scooter;

Scooter* createScooter(int wheels, int power);
void saveScooter(FILE* f, Scooter* scooter);

