#pragma once

#include <stdio.h>

#define CAR 2

typedef struct{
	int wheels;
	int power;
	int mark;
} Car;

Car* createCar(int wheels, int power);
void saveCar(FILE* f, Car* car);

