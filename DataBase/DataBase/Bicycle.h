#pragma once

#include <stdio.h>

#define BICYCLE 0

typedef struct{
	int wheels;
	int power;
} Bicycle;

Bicycle* createBicycle(int wheels, int power);
void saveBicycle(FILE* f, Bicycle* bicycle);

