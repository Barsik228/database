#include "Scooter.h"
Scooter* createScooter(int wheels, int power){
	Scooter* newScooter;
	newScooter->wheels = wheels;
	newScooter->power = power;
	return newScooter;
}

void saveScooter(FILE* f, Scooter* scooter){
	fprintf(f, "%d %d %d", SCOOTER, scooter->wheels, scooter->power);
	fclose(f);
}