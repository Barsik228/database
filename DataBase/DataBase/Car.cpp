#include "Car.h"
Car* createCar(int wheels, int power){
	Car* newCar;
	newCar->wheels = wheels;
	newCar->power = power;
	return newCar;
}

void saveCar(FILE* f, Car* car){
	fprintf(f, "%d %d %d", CAR, car->wheels, car->power);
	fclose(f);
}