#pragma once

#include "Bicycle.h"
#include "Bike.h"
#include "Car.h"
#include "Scooter.h"
#include "Sportcar.h"

typedef struct{
	Element* head;
	int length;
} Base;

typedef struct{
	void* machine;
	int type;
	int id;
	Element* next;
} Element;

Base* createBase(void* head, int length);
Element* createElement(void* machine, int type, int id, Element* next);
Base* addElement(Base* base, Element* elem);
void removeElement(int id);
void saveBase(FILE* f, Base* base);
Base* loadBase(char* path);